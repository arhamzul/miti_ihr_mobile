import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicStorageModule} from '@ionic/storage';
import {HttpModule} from "@angular/http";  // yg ni


import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ChainDropdownPage} from "../pages/chain-dropdown/chain-dropdown";
import {DashboardPage} from "../pages/dashboard/dashboard";
import {DateRangePage} from "../pages/date-range/date-range";
import {LoginPage} from "../pages/login/login";
import {PaginationPage} from "../pages/pagination/pagination";
import {ResetPasswordPage} from "../pages/reset-password/reset-password";
import {InfoPage} from "../pages/info/info";
import {PassParameterPage} from "../pages/pass-parameter/pass-parameter";
import {ReceiveParameterPage} from "../pages/receive-parameter/receive-parameter";
import {CrudPage} from "../pages/crud/crud";
import {ArnabKucingPage} from "../pages/arnab-kucing/arnab-kucing";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ChainDropdownPage,
    DashboardPage,
    DateRangePage,
    LoginPage,
    PaginationPage,
    ResetPasswordPage,
    InfoPage,
    PassParameterPage,
    ReceiveParameterPage,
    CrudPage,
    ArnabKucingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule  // yg ni
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ChainDropdownPage,
    DashboardPage,
    DateRangePage,
    LoginPage,
    PaginationPage,
    ResetPasswordPage,
    InfoPage,
    PassParameterPage,
    ReceiveParameterPage,
    CrudPage,
    ArnabKucingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
