import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {LoginPage} from "../pages/login/login";
import {ResetPasswordPage} from "../pages/reset-password/reset-password";
import {PaginationPage} from "../pages/pagination/pagination";
import {DashboardPage} from "../pages/dashboard/dashboard";
import {ChainDropdownPage} from "../pages/chain-dropdown/chain-dropdown";
import {DateRangePage} from "../pages/date-range/date-range";
import {PassParameterPage} from "../pages/pass-parameter/pass-parameter";
import {CrudPage} from "../pages/crud/crud";
import {ArnabKucingPage} from "../pages/arnab-kucing/arnab-kucing";
import {Storage} from "@ionic/storage";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = DateRangePage;

  // declare array
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public storage: Storage) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Login', component: LoginPage },
      { title: 'Reset Password', component: ResetPasswordPage },
      { title: 'Date Range', component: DateRangePage },
      { title: 'Chain Dropdown', component: ChainDropdownPage },
      { title: 'Dashboard', component: DashboardPage },
      { title: 'Pagination', component: PaginationPage },
      { title: 'Pass Parameter', component: PassParameterPage },
      { title: 'CRUD', component: CrudPage },
      { title: 'Arnab Kucing', component: ArnabKucingPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  doLogout() {
    // set value rememberMe kepada false
    this.storage.set('rememberMe', 'false');
    this.nav.setRoot(LoginPage);
  }
}
