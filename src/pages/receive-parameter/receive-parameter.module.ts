import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiveParameterPage } from './receive-parameter';

@NgModule({
  declarations: [
    ReceiveParameterPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceiveParameterPage),
  ],
})
export class ReceiveParameterPageModule {}
