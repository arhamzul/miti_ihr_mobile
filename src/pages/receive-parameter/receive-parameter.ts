import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReceiveParameterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receive-parameter',
  templateUrl: 'receive-parameter.html',
})
export class ReceiveParameterPage {
  sendData : string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.sendData =this.navParams.get('dataSaya');
    // console.log(this.navParams.get('myText'))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiveParameterPage');
  }

}
