import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChainDropdownPage } from './chain-dropdown';

@NgModule({
  declarations: [
    ChainDropdownPage,
  ],
  imports: [
    IonicPageModule.forChild(ChainDropdownPage),
  ],
})
export class ChainDropdownPageModule {}
