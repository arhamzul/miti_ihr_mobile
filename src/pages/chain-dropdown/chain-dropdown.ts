import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {PaginationPage} from "../pagination/pagination";
import {ResetPasswordPage} from "../reset-password/reset-password";
import {LoginPage} from "../login/login";
import {PassParameterPage} from "../pass-parameter/pass-parameter";
import {CrudPage} from "../crud/crud";
import {DateRangePage} from "../date-range/date-range";
import {DashboardPage} from "../dashboard/dashboard";
import {Http} from "@angular/http";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ChainDropdownPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chain-dropdown',
  templateUrl: 'chain-dropdown.html',
})
export class ChainDropdownPage {
  response: any;
  token: string;
  // senarai data yang dapat dari API
  countries: any;
  states: any;
  cities: any;

  // data yang kita nak send kepada API (based on apa yang user select)
  country: string;
  state: string;
  city: string;

  // for sample learning purpose
  colors: Array<{ nama: string, name: string }>;
  warna: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public http: Http,
              public storage: Storage) {
    // get token
    // this.getToken();
    // get list of countries untuk populate dropdown pertama
    this.getAllCountries();

    // initialize color value untuk dropdown ke 4
    this.colors = [
      {nama: 'Merah', name: 'Red'},
      {nama: 'Biru', name: 'Blue'},
      {nama: 'Hijau', name: 'Green'},
      {nama: 'Kuning', name: 'Yellow'},
      {nama: 'Hitam', name: 'Black'},
      {nama: 'Ungu', name: 'Purple'}
    ];


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChainDropdownPage');
  }

  // get token from storage and store in variable. Needed
  // getToken() {
  //   this.storage.get('token').then((val) => {
  //     console.log('Token : ', val);
  //     this.token = val;
  //   });
  // }

  getAllCountries() {
    // first get token from storage. Api perlukan token
    this.storage.get('token').then((val) => {
      console.log('Token : ', val);
      this.token = val;

      // once dah dapat token dari storage, kita get data dari api
      var url = 'http://127.0.0.1:8000/api/country/all';

      var sendData = {
        "token": this.token
      };

      this.http.post(url, sendData)
        .subscribe(data => {
          this.response = data.json();
          console.log(this.response);

          if (this.response.status == 'ok') {
            this.countries = this.response.countries;
          } else {
            alert (this.response.message);
          }
        }, error => {
          alert('Tidak dapat hubungi server');
        });
    });
  }

  selectCountry() {
    // capture value dropdown country
    // send to API, value country
    // get list of states based on the country value
    // populate second dropdown using list of states
    console.log(this.country);

    var url = 'http://127.0.0.1:8000/api/state/by-country';

    var sendData = {
      "token" : this.token,
      "countryId": this.country
    };

    this.http.post(url, sendData)
      .subscribe(data => {
        this.response = data.json();
        console.log(this.response);

        if (this.response.status == 'ok') {
          this.states = this.response.states;
        } else {
          alert (this.response.message);
        }
      }, error => {
        alert('Tidak dapat hubungi server');
      });
  }

  selectState() {
    var url = 'http://127.0.0.1:8000/api/city/by-state';

    var sendData = {
      "token" : this.token,
      "stateId": this.state
    };

    this.http.post(url, sendData)
      .subscribe(data => {
        this.response = data.json();
        console.log(this.response);

        if (this.response.status == 'ok') {
          this.cities = this.response.cities;
        } else {
          alert (this.response.message);
        }
      }, error => {
        alert('Tidak dapat hubungi server');
      });
  }



  // -------------------- learning aje ---
  showColor() {
    // alert("Anda memilih warna : " + this.warna);
    const alert = this.alertCtrl.create({
      title: 'Berjaya',
      subTitle: 'Anda pilih warna ' + this.warna,
      buttons: ['Baik']
    });
    alert.present();
  }

}
