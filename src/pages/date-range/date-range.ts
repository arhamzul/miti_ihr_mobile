import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from "@angular/http";

/**
 * Generated class for the DateRangePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-date-range',
  templateUrl: 'date-range.html',
})
export class DateRangePage {
  startDate: string;
  endDate: string;
  response: any;
  rekodKami: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DateRangePage');
  }

  doSearch() {
    console.log('Start Date: ' + this.startDate);
    console.log('End Date: ' + this.endDate);

    var url = 'http://127.0.0.1:8000/api/date/search';

    var sendData = {
      "startDate": this.startDate,
      "endDate": this.endDate
    };

    this.http.post(url, sendData)
      .subscribe(data => {
        this.response = data.json();
        console.log(this.response);
        this.rekodKami = this.response.records;

      }, error => {
        alert('Tidak dapat hubungi server');
      });
  }

}
