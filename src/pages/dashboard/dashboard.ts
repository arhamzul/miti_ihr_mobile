import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {InfoPage} from "../info/info";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage) {
    storage.get('rememberMe').then((val) => {
      console.log('Remember me : ', val);
    });

    // get token from storage
    storage.get('token').then((val) => {
      console.log('Token : ', val);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  doShowInfo() {
    this.navCtrl.push(InfoPage);
  }

  doShowInfo2() {
    this.navCtrl.setRoot(InfoPage);
  }

}
