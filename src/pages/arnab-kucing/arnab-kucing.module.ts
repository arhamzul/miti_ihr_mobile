import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArnabKucingPage } from './arnab-kucing';

@NgModule({
  declarations: [
    ArnabKucingPage,
  ],
  imports: [
    IonicPageModule.forChild(ArnabKucingPage),
  ],
})
export class ArnabKucingPageModule {}
