import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ArnabKucingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-arnab-kucing',
  templateUrl: 'arnab-kucing.html',
})
export class ArnabKucingPage {
  gambar : string = 'assets/imgs/choose.jpg';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArnabKucingPage');
  }

  showAnimal(binatang) {
    if (binatang == 'Arnab') {
      console.log('Rabbit yoo');
      this.gambar = 'assets/imgs/rabbit.jpg';
    }

    if (binatang == 'Kucing') {
      console.log('Meeow');
      this.gambar = 'assets/imgs/cat.jpg';
    }

    if (binatang == 'Buaya') {
      this.gambar = 'http://animals.sandiegozoo.org/sites/default/files/2016-10/alligator_american.jpg';
    }
  }

}
