import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from "@angular/http";

/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  email: string = '';
  response:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: Http,  // yg ni
  ) {
    // run these things while this page is being constructed
    // console.log(this.email);
  }

  ionViewDidLoad() {
    // run these things once the page has been loaded
    console.log('ionViewDidLoad ResetPasswordPage');
    // console.log(this.email);
  }

  // run bila user click reset button
  doReset() {
    var url = 'http://127.0.0.1:8000/api/reset-password';

    var sendData = { "email": this.email };

    this.http.post(url, sendData)
      .subscribe(data => {
        this.response = data.json();
        alert(this.response);

      }, error => {
        alert('Tidak dapat hubungi server');
      });

  }

}
