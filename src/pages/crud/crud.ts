import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the CrudPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crud',
  templateUrl: 'crud.html',
})
export class CrudPage {
  nama:string;
  umur: number;
  jantina: string;
  kakitangan: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrudPage');
  }

  doAdd() {
    var person = {
      "nama" : this.nama,
      "umur" : this.umur,
      "jantina" : this.jantina
    };

    console.log(person);

    this.kakitangan.push(person);
    console.log(this.kakitangan);
  }

  doDelete(staff) {
    console.log(staff);
    let index = this.kakitangan.indexOf(staff);
    console.log(index);

    this.kakitangan.splice(index, 1);
  }

  doAsk(staff) {
    const confirm = this.alertCtrl.create({
      title: 'Pasti Padam?',
      message: 'Adakah anda benar-benar ingin memadam rekod ini? :: ' + staff.nama,
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Batal clicked');
          }
        },
        {
          text: 'Ya. Sila Padam',
          handler: () => {
            // console.log('Agree clicked');
            this.doDelete(staff);

          }
        }
      ]
    });
    confirm.present();
  }

}
