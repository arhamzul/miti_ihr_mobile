import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ReceiveParameterPage} from "../receive-parameter/receive-parameter";

/**
 * Generated class for the PassParameterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pass-parameter',
  templateUrl: 'pass-parameter.html',
})
export class PassParameterPage {
  myData : string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PassParameterPage');
  }

  doSend() {
    // alert(this.myData);
    this.navCtrl.push(ReceiveParameterPage, {'dataSaya' : this.myData });
    // this.navCtrl.push(ReceiveParameterPage, {'myText' : 'Hello World' });
  }

}
