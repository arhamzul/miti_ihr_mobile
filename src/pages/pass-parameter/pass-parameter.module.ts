import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassParameterPage } from './pass-parameter';

@NgModule({
  declarations: [
    PassParameterPage,
  ],
  imports: [
    IonicPageModule.forChild(PassParameterPage),
  ],
})
export class PassParameterPageModule {}
