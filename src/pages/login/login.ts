import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {DashboardPage} from "../dashboard/dashboard";
import {Storage} from '@ionic/storage';
import {Http} from "@angular/http";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  // declare variable (attribute) yang nak digunakan oleh class ini
  username: string = '';
  password: string = '';
  response: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              private storage: Storage,
              private http: Http) {
    this.storage.get('rememberMe').then((val) => {
      console.log(val);
      if (val == 'true') {
        this.navCtrl.setRoot(DashboardPage);
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  doLogin() {
    // username : user
    // password : pass
    // if username and password valid,
    //    store remember me in storage
    //    redirect to dashboardPage
    // else, show ionic alert, mention invalid credential

    var url = 'http://127.0.0.1:8000/api/login';

    var sendData = {
      "username": this.username,
      "password": this.password,
    };

    this.http.post(url, sendData)
      .subscribe(data => {
        this.response = data.json();
        console.log(this.response);

        var status = this.response.status;
        if (status == 'pass') {
          // save token info
          this.storage.set('token', this.response.token);
          // remember me
          this.storage.set('rememberMe', 'true').then(() => {
            this.navCtrl.setRoot(DashboardPage);
          });
        } else {
          const alert = this.alertCtrl.create({
            title: 'Login Error',
            subTitle: this.response.message,
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        alert('Tidak dapat hubungi server');
      });


    // if (this.username == 'user' && this.password == 'pass') {
    //   // remember me
    //   this.storage.set('rememberMe', 'true').then(() => {
    //     this.navCtrl.setRoot(DashboardPage);
    //   });
    //
    // } else {
    //   const alert = this.alertCtrl.create({
    //     title: 'Login Error',
    //     subTitle: 'Invalid Credential',
    //     buttons: ['OK']
    //   });
    //   alert.present();
    // }
  }

}
